
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.mail.Address;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class xml {



    public xml() {
    }

    void guardarVariables(List<String> datosList) {


        DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder icBuilder;
        try {
            icBuilder = icFactory.newDocumentBuilder();
            Document doc = icBuilder.newDocument();
            Element mainRootElement = doc.createElement("Variables");
            doc.appendChild(mainRootElement);



            List<Variable> listVariables = leerVariables();


            // append child elements to root element
            mainRootElement.appendChild(getVariables(doc, datosList.get(0), datosList.get(1), datosList.get(2), datosList.get(3), datosList.get(4) , datosList.get(5), datosList.get(6)));
            for (Variable v: listVariables){

                mainRootElement.appendChild(getVariables(doc,v.getTimestamp(),v.getTemperatura(),v.getTension(),v.getCorriente(),v.getPotencia(),v.getPresion(),v.getRemitente()));

            }

            // output DOM XML to console
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult console = new StreamResult(Constantes.PATHVARIABLES);



            File file = new File("/Users/Federico/Desktop/Escritorio/Facultad/SistDeTransmisionRepo/SistProyecto/archivoVariables.xml");

            StreamResult archivoLocal = new StreamResult(file);

          // guarda en archivo de escritorio
            transformer.transform(source, console);


            System.out.println("pasoooo");
            //guarda en archivo local
            transformer.transform(source, archivoLocal);


            System.out.println("\n variables guardadas");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    void guardarRemitentes() {

        DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder icBuilder;
        try {
            icBuilder = icFactory.newDocumentBuilder();
            Document doc = icBuilder.newDocument();
            Element mainRootElement = doc.createElement("Remitentes");
            doc.appendChild(mainRootElement);

            // append child elements to root element
            mainRootElement.appendChild(getRemitentes(doc,  "federicopuy@gmail.com"));
            mainRootElement.appendChild(getRemitentes(doc, "arielniscola@gmail.com"));

            // output DOM XML to console
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult console = new StreamResult(Constantes.PATHREMITENTES);
            transformer.transform(source, console);

            System.out.println("\n Remitentes Guardados");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Node getVariables(Document doc, String timestamp, String Temperatura, String Tension, String Corriente, String Potencia, String Presion,  String Remitente) {
        Element remit = doc.createElement("Variable");

        remit.appendChild(getRemitentesElements(doc, remit, "Timestamp", timestamp));
        remit.appendChild(getRemitentesElements(doc, remit, "Temperatura", Temperatura));
        remit.appendChild(getRemitentesElements(doc, remit, "Tension", Tension));
        remit.appendChild(getRemitentesElements(doc, remit, "Corriente", Corriente));
        remit.appendChild(getRemitentesElements(doc, remit, "Potencia", Potencia));
        remit.appendChild(getRemitentesElements(doc, remit, "Presion", Presion));
        remit.appendChild(getRemitentesElements(doc, remit, "Remitente", Remitente));


        return remit;
    }

    public String obtenerRemitente(Address address) {

        String remitenteTodoJunto = address.toString();
        String remitenteSolo = remitenteTodoJunto;
        remitenteSolo = remitenteSolo.substring(remitenteSolo.indexOf("<") + 1);
        remitenteSolo = remitenteSolo.substring(0, remitenteSolo.indexOf(">"));
        System.out.println("Remitente Solo" + remitenteSolo);

        return remitenteSolo;
    }


    public static Node getRemitentes(Document doc, String email) {
        Element remit = doc.createElement("Remitente");

        remit.appendChild(getRemitentesElements(doc, remit, "Email", email));

        return remit;
    }
    // utility method to create text node
    public static Node getRemitentesElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }


    public List<String> leerRemitentes(){
        List<String> remitentes = new ArrayList<String>();
        try {
        File fXmlFile = new File(Constantes.PATHREMITENTES);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = null;

            doc = dBuilder.parse(fXmlFile);


        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        doc.getDocumentElement().normalize();

        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());


        NodeList nList = doc.getElementsByTagName("Remitente");

        System.out.println("----------------------------");

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
               remitentes.add(eElement.getElementsByTagName("Email").item(0).getTextContent());
            }
        }

    } catch (Exception e) {
        e.printStackTrace();
    }
    return remitentes;
}



    public List<Variable> leerVariables(){

        List<Variable> variables = new ArrayList<Variable>();


        try {
            File fXmlFile = new File(Constantes.PATHVARIABLES);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = null;

            doc = dBuilder.parse(fXmlFile);


            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());


            NodeList nList = doc.getElementsByTagName("Variable");

            System.out.println("----------------------------");
            System.out.println("tamano "+nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                System.out.println("temp");
                Variable variable = new Variable();

                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    variable.setTimestamp(eElement.getElementsByTagName("Timestamp").item(0).getTextContent());
                    variable.setTemperatura(eElement.getElementsByTagName("Temperatura").item(0).getTextContent());
                    variable.setTension(eElement.getElementsByTagName("Tension").item(0).getTextContent());
                    variable.setCorriente(eElement.getElementsByTagName("Corriente").item(0).getTextContent());
                    variable.setPotencia(eElement.getElementsByTagName("Potencia").item(0).getTextContent());
                    variable.setPresion(eElement.getElementsByTagName("Presion").item(0).getTextContent());
                    variable.setRemitente(eElement.getElementsByTagName("Remitente").item(0).getTextContent());

                }

                variables.add(variable);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return variables;
    }




}
