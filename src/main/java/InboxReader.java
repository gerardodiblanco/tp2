/**
 * Created by Federico on 10/26/17.
 */

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import org.apache.log4j.Logger;
import org.apache.log4j.*;
import org.apache.log4j.Level;



import static com.sun.deploy.util.BufferUtil.MB;
import static java.lang.System.out;

public class InboxReader {



    static  Logger log = Logger.getLogger(InboxReader.class);


    xml rwXML = null;

    public InboxReader() {
        rwXML = new xml();
    }

    public void leerEmail(final String userName, final String password) {




        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");

        SimpleLayout layout = new SimpleLayout();
        RollingFileAppender appender = null;
        try {
            appender = new RollingFileAppender(layout,Constantes.PATHLOGFILE,false);


        } catch (IOException e) {
            e.printStackTrace();

        }


     //   log.setLevel((Level) Level.DEBUG);

        setearElLog(log, appender);




        //org.apache.log4j.BasicConfigurator.configure();


        try {



            Properties properties = new Properties();

            // server setting
            properties.put("mail.pop3.host", "pop.gmail.com");
            properties.put("mail.pop3.port", "995");

            // SSL setting
            properties.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.setProperty("mail.pop3.socketFactory.fallback", "false");
            properties.setProperty("mail.pop3.socketFactory.port", String.valueOf("995"));

            Session session = Session.getDefaultInstance(properties);

            Store store = session.getStore("pop3");
            store.connect(userName, password);

            // opens the inbox folder
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);

            // fetches new messages from server
            Message[] messages = folderInbox.getMessages();


         //   Message messages[] = inbox.getMessages();
            List<String> datos = null;



            for (Message message : messages) {



                try {
                    datos = new ArrayList<String>();
                    out.println(message.getSubject());
                    Address[] remitenteAddress = message.getFrom();
                    String remitente = rwXML.obtenerRemitente(remitenteAddress[0]);




                    if(validarRemitente(remitente)) {

                        InputStream in = message.getInputStream();

                        StringWriter writer = new StringWriter();
                        IOUtils.copy(in, writer, "UTF-8");
                        String mensaje = writer.toString();


                        mensaje = mensaje.substring(mensaje.indexOf(">") + 1);
                        mensaje = mensaje.substring(0, mensaje.indexOf("<"));
                        System.out.println("Mensaje" + mensaje);

                        if (validarRegex(mensaje)) {

                            log.info("Mensaje Recibido correctamente");

                            System.out.println("--------------------------------------------------------------------------");

                            datos.add(mensaje.substring(0, mensaje.indexOf(";")));

                            for (int i = 0; i < 5; i++) {
                                mensaje = mensaje.substring(mensaje.indexOf(";") + 1);
                                datos.add(mensaje.substring(0, mensaje.indexOf(";")));
                            }


                            datos.add(remitente);
                            
                            out.println("---------------------------------------------------------------------------------------------------------------------------------------------");


// guardar datos y errores en archivos separados


   // loguear inicio y fin




                            try {


                                rwXML.guardarVariables(datos);
                                //rwXML.guardarRemitentes();
                                //rwXML.leerRemitentes();

                            } catch (Exception e){

                                log.error(e);

                            }


                            folderInbox.close();
                            store.close();


                        }


                    }



                } catch (Exception e) {
                   // e.printStackTrace();
                    log.error(e);
                }
            }
        } catch (Exception e) {

            log.error(e);


        }
    }





    public void setearElLog(Logger log, RollingFileAppender appender){

        log.addAppender(appender);

//        log.debug("Sample debug message");
        log.info("Reintenta" );
//        log.warn("Sample warn message");
//        log.error("Sample error message");
//        log.fatal("Sample fatal message");




    }

    private boolean validarRegex(String mensaje) {


        String regexValido = "\\b\\d{4}[-]?\\d{2}[-]?\\d{2} \\b\\d{2}[:]\\d{2}[:]\\d{2}[.]\\d{3}[;]\\d+[;]\\d+[;]\\d+[;]\\d+[;]\\d+[;]";


        Pattern patron1 = Pattern.compile(regexValido);
        Matcher patenteCaracteresValidos = patron1.matcher(mensaje);

        if (patenteCaracteresValidos.matches()) {

            System.out.println("Regex Valido");
            return true;

        } else {

            System.out.println("Regex Invalido");
            return false;
        }
    }


    boolean validarRemitente(String remitente) {
        boolean valido = false;

        List<String> remitentes = rwXML.leerRemitentes();

        for (String remitenteValido : remitentes) {
            if (remitenteValido.equals(remitente)) {
                System.out.println("remitente valido " + remitente);
                valido = true;
                break;
            }
        }

        return valido;

    }


}







